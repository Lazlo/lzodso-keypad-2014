# lzoDSO Keypad

Contains schematics and firmware for the lzoDSO keypad.

This repository supersedes [lzodso-keypad-2013](https://gitlab.com/Lazlo/lzodso-keypad-2013).

## Hardware

The keypad consists of 15 push-buttons and 8 rotary encoders each with a built-in push-button.

It is built using three circuit boards. The board on the top of the
stack that holds the button and encoders is the __panel__. Underneath
is the __processing__ board that contains shift registers to reduce the
number of pins required to interface with the push-buttons and encoders.
At the bottom of the stack is the __busif__ board that contains a micro-controller
that performs periodic scanning of the controls and makes them available
over SPI or I2C.

The micro-controller used is a ATmega8 running at 20 MHz and 5 volt logic level.

The schematics are available in the [schematics](schematics) directory.

![photo of lzoDSO keypad hardware v1](doc/pictures/P1070919-keypad-small.JPG)

Mapping of push-buttons keys:

```
+--------------------+
|   25    1 2 3 4 5  |
|         6 7 8 9 10 |
|   12               |
|11    13    14    15|
|   16               |
|21    18    19    20|
|22    23    24    17|
+--------------------+
```

## Firmware

To compile the firmware and upload it to the hardware:

```
make
make program
```

The UART of the micro-controller is configured to operate at 115200 bps.
