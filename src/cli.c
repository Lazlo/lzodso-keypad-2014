/* cli.c */

#include "cli.h"

#include "config.h"

#ifdef CONFIG_DEBUG_CLI
#include "syslog.h"
#define cldbg	syslog
#else
#define cldbg	(void)
#endif

#include "rb.h"

struct cli_io_buf_s {
	struct rb_s tx;
	struct rb_s rx;
};

struct cli_io_handler_s {
	void (*putc)(char c);
	char (*getc)(void);
};

struct cli_s {
	struct cli_io_buf_s	buf;
	struct cli_io_handler_s	handler;
};

static struct cli_s g_cli;

void cli_init(void (*putc)(char c), char (*getc)(void))
{
	g_cli.handler.putc = putc;
	g_cli.handler.getc = getc;
}

void cli_update(void)
{
	struct rb_s *b;

	cldbg("c\n\r");

	/* TODO Check if there was a byte received */
	b = &g_cli.buf.rx;
	if (b->tail < b->head)
	{
		g_cli.handler.putc(b->buf[b->tail++]);
	}
	else
	{
		b->tail = b->head = 0;
	}

#if 0
	/* TODO Check if a byte is to be transmitted */
	b = &g_cli.buf.tx;
	if (b->tail < b->head)
	{
		b->buf[b->tail++] = g_cli.handler.getc();

		if (b->tail == b->head)
			b->tail = b->head = 0;
	}
#endif

#if 0
	char c;

	/* TODO Display prompt */
	(*s_putc)('>');
	(*s_putc)(' ');

	/* TODO Wait for character to read from serial */
	while ((c = (*s_getc)()) != '\r')
	{
		(*s_putc)(c);
	}
	(*s_putc)('X');
	(*s_putc)('\r');
	(*s_putc)('\n');
#endif
	/* TODO Echo characters back to serial */
	/* TODO Parse line */
	/* TODO Setup command */
	/* TODO Execute command */
	/* TODO Check command result */
}
