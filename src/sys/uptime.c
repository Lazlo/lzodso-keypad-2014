/* uptime.c */

#include "uptime.h"

#include <stdint.h>

#define SECS_PER_MIN	60
#define MINS_PER_HOUR	60
#define HOURS_PER_DAY	24

struct uptime_s {
	uint8_t h;	/**< Hours */
	uint8_t m;	/**< Minutes */
	uint8_t s;	/**< Seconds */
	char str[9];	/**< C-string of time (HH:MM:SS\0). */
};

static struct uptime_s g_uptime = {
	.h = 0,
	.m = 0,
	.s = 0,
	.str = {
		[2] = ':',
		[5] = ':',
		[8] = '\0',
	},
};

static void tick(struct uptime_s *u);
static void tostr(struct uptime_s *u);

static void tick(struct uptime_s *u)
{
	if (++u->s == SECS_PER_MIN) {
		u->s = 0;
		if (++u->m == MINS_PER_HOUR) {
			u->m = 0;
			if (++u->h == HOURS_PER_DAY) {
				u->h = 0;
			}
		}
	}
}

static void tostr(struct uptime_s *u)
{
	u->str[0] = '0' + (u->h / 10);
	u->str[1] = '0' + (u->h % 10);

	u->str[3] = '0' + (u->m / 10);
	u->str[4] = '0' + (u->m % 10);

	u->str[6] = '0' + (u->s / 10);
	u->str[7] = '0' + (u->s % 10);
}

#include "syslog.h"

void uptime_show(void)
{
	struct uptime_s *u = &g_uptime;

	tick(u);
	tostr(u);

	syslog(u->str);
	syslog("\r\n");
}
