/* ldk.c */

#include "ldk.h"

#include "config.h"

#ifdef CONFIG_DEBUG_LDK
#include "syslog.h"
#define ldkdbg syslog
#else
#define ldkdbg (void)
#endif

#include "buf.h"
#include "rb.h"

struct ldk_s {
	const struct ldk_ioop_s *io;
	void (*fp_enqueue_key)(const uint8_t);

	uint8_t mx_cols;
	uint8_t mx_buflen;
	uint8_t *mx_inbuf;
	uint8_t *mx_inlastbuf;

	uint8_t enc_knobs;
	uint8_t enc_buflen;
	uint8_t *enc_inbuf;
	uint8_t *enc_inlastbuf;
};

static void ldkdumpbuf(uint8_t *buf, const uint8_t len);
static uint8_t mx_readbyte(const struct ldk_ioop_s *io);
static uint8_t enc_readbyte(const struct ldk_ioop_s *io);
static void scan(struct ldk_s *k);
static uint8_t changed(struct ldk_s *lk);

#define LDK_MX_BUFSIZE	5
#define LDK_ENC_BUFSIZE	2

static uint8_t s_mx_inbuf[LDK_MX_BUFSIZE];
static uint8_t s_mx_inlastbuf[LDK_MX_BUFSIZE];
static uint8_t s_enc_inbuf[LDK_ENC_BUFSIZE];
static uint8_t s_enc_inlastbuf[LDK_ENC_BUFSIZE];

/* TODO Store encoding properties of rotary encoders */
/* TODO Have mapping from encoding property to knob */

static struct ldk_s s_ldk = {
	.io		= 0,
	.mx_cols	= 5,
	.mx_buflen	= LDK_MX_BUFSIZE,
	.mx_inbuf	= s_mx_inbuf,
	.mx_inlastbuf	= s_mx_inlastbuf,
	.enc_knobs	= 8,
	.enc_buflen	= LDK_ENC_BUFSIZE,
	.enc_inbuf	= s_enc_inbuf,
	.enc_inlastbuf	= s_enc_inlastbuf,
};

static void ldkdumpbuf(uint8_t *buf, const uint8_t len)
{
	ldkdbg("ldk scan ");
#ifdef CONFIG_DEBUG_LDK
	dumpbuf(buf, len, ldkdbg);
#endif
	ldkdbg("\r\n");
}

static uint8_t mx_readbyte(const struct ldk_ioop_s *io)
{
	uint8_t b;

	(io->fp_piso_latch)();			/* Read column */
	b = (io->fp_piso_readbyte)();		/* Save column data for later processing */
	(io->fp_sipo_clock)();			/* Advance clock to select next column */

	return b;
}

static uint8_t enc_readbyte(const struct ldk_ioop_s *io)
{
	uint8_t b;

	b = (io->fp_piso_readbyte)();

	return b;
}

static void scan(struct ldk_s *k)
{
	const struct ldk_ioop_s *io;
	uint8_t *buf;
	uint8_t len;
	uint8_t i;

	io = k->io;

	/* Reset the address logic of the switching matrix. */
	(io->fp_sipo_reset)();
	(io->fp_sipo_write)(1);

	/* Read the switching matrix */
	buf = k->mx_inbuf;
	len = k->mx_buflen;
	for (i = 0; i < len; i++)
		buf[i] = mx_readbyte(io);

	/* Read the rotary encoders */
	buf = k->enc_inbuf;
	len = k->enc_buflen;
	for (i = 0; i < len; i++)
		buf[i] = enc_readbyte(io);
}

static uint8_t changed(struct ldk_s *lk)
{
	uint8_t nchanged = 0;
	uint8_t len;
	uint8_t i; /* equvalent to the active column in the matrix */
	uint8_t k;
	uint8_t mask;
	uint8_t *currbuf;
	uint8_t *lastbuf;
	uint8_t curr;
	uint8_t last;
	uint8_t down;
	uint8_t key;
	uint8_t key_code;

	len	= lk->mx_buflen;
	currbuf	= lk->mx_inbuf;
	lastbuf	= lk->mx_inlastbuf;
	for (i = 0; i < len; i++) {
		for (k = 5; k > 0; k--) {
			mask = (1 << (k - 1));
			curr = (currbuf[i] & mask) ? 1 : 0;
			last = (lastbuf[i] & mask) ? 1 : 0;
			if (curr == last)
				continue;
			down = curr;
			key = (5 * (k - 1)) + (i + 1);
			key_code = (2 * key);
			if (down)
				key_code -= 1;
			(lk->fp_enqueue_key)(key_code);
			nchanged++;
		}
	}

	const uint8_t keyoffset = 50;

	len	= lk->enc_buflen;
	currbuf	= lk->enc_inbuf;
	lastbuf	= lk->enc_inlastbuf;
	for (i = 0; i < len; i++) {
		/* TODO Instead of checking every bit we need to check pairs of two
		 * bits. If the pair changed then we need to decode the direction,
		 * based on the specific encoding properties of the rotary encoder
		 * that has changed. */
		for (k = 8; k > 0; k--) {
			mask = (1 << (k - 1));
			curr = (currbuf[i] & mask);
			last = (lastbuf[i] & mask);
			if (curr == last)
				continue;
			key = 1 + (len * 8) - ((i * 8) + k); /* reverse order of knobs */
			key += keyoffset;
			(lk->fp_enqueue_key)(key);
			nchanged++;
		}
	}

	return nchanged;
}

void ldk_init(const struct ldk_ioop_s *io, void (*enqueue_key)(const uint8_t))
{
	struct ldk_s *lk = &s_ldk;

	/* Store pointer to IO operations struct */

	lk->io = io;

	/* Save function pointer to higher level enqueue key function */

	lk->fp_enqueue_key = enqueue_key;

	/* Reset current and last buffer */

	clearbuf(lk->mx_inbuf, lk->mx_buflen);
	clearbuf(lk->mx_inlastbuf, lk->mx_buflen);

	/* Reset the rotary encoder input buffer */
	clearbuf(lk->enc_inbuf, lk->enc_buflen);
	clearbuf(lk->enc_inlastbuf, lk->enc_buflen);

	scan(lk);		/* Perform initial scan */
}

void ldk_scan(void)
{
	struct ldk_s *lk = &s_ldk;
	uint8_t len;
	uint8_t *buf;
	uint8_t *lastbuf;

	/* Save 'current' (from last call) as 'last' for switching matrix
	 * input buffers */

	len	= lk->mx_buflen;
	buf	= lk->mx_inbuf;
	lastbuf	= lk->mx_inlastbuf;

	movebytes(lastbuf, buf, len);

	/* Save 'current' (from last call) as 'last' for rotary encoder
	 * input buffers */

	len	= lk->enc_buflen;
	buf	= lk->enc_inbuf;
	lastbuf	= lk->enc_inlastbuf;

	movebytes(lastbuf, buf, len);

	/* Perform the scan operation (and save state in respective inbuf) */

	scan(lk);
	ldkdumpbuf(buf, len);		/* Display current buf contents */
}

uint8_t ldk_changed(void)
{
	return changed(&s_ldk);
}
