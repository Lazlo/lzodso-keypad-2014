/* buf.c */

#include "buf.h"

void clearbuf(uint8_t *buf, const uint8_t len)
{
	uint8_t i;

	for (i = 0; i < len; i++)
		buf[i] = 0;
}

void cpybuf(uint8_t *to, const uint8_t *from, const uint8_t len)
{
	uint8_t i;

	for (i = 0; i < len; i++)
		to[i] = from[i];
}

void movebytes(uint8_t *to, uint8_t *from, const uint8_t len)
{
	cpybuf(to, from, len);
	clearbuf(from, len);
}

void dumpbuf(const uint8_t *buf, const uint8_t len, void (*fp_puts)(char *))
{
	uint8_t i;
	uint8_t k;

	for (i = 0; i < len; i++) {
		/* Display bits in byte starting from left (MSB) */
		for (k = 8; k > 0; k--) {
			(*fp_puts)((buf[i] & (1 << (k - 1))) ? "1" : "0");
		}
		if (i < (len-1))
			(*fp_puts)(" ");
	}
}
