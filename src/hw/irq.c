/* irq.c */

#include "irq.h"

void irq_enable(bool on)
{
#	include <avr/interrupt.h>

	if (on)	sei();
	else	cli();
}
