/* sipo.c */

#include "sipo.h"

#include "gpio.h"

struct sipo_s {
	struct pin_s *pin_dataout;	/**< Serial output pin. */
	struct pin_s *pin_clock;	/**< Serial clock output pin. */
	struct pin_s *pin_clear;	/**< Pin to clear shift register. */
};

static const uint8_t delay_resethold_us	= 1;
static const uint8_t delay_clockhold_us	= 1;
static const uint8_t delay_datahold_us	= 1;

static struct sipo_s g_sipo;

static void udelay(const uint8_t delay_us)
{
	// TODO Implement delay
}

static void reset(void)
{
	gpio_writepin(g_sipo.pin_clear, false);
	udelay(delay_resethold_us);
	gpio_writepin(g_sipo.pin_clear, true);
}

static void advance_clock(void)
{
	gpio_writepin(g_sipo.pin_clock, true);
	udelay(delay_clockhold_us);
	gpio_writepin(g_sipo.pin_clock, false);
}

static void writebit(const bool bit)
{
	if (bit)
		gpio_writepin(g_sipo.pin_dataout, true);
	udelay(delay_datahold_us);
	sipo_clock();
	gpio_writepin(g_sipo.pin_dataout, false);
}

void sipo_init(struct pin_s *pin_dataout,
		struct pin_s *pin_clock,
		struct pin_s *pin_clear)
{
	pin_t *p;

	g_sipo.pin_dataout = pin_dataout;
	g_sipo.pin_clock = pin_clock;
	g_sipo.pin_clear = pin_clear;

	/* SIPO DATAOUT */

	p = pin_dataout;
	gpio_setpinconfig(p, true, false, false);	// output, no pullup, no pulldown
	gpio_writepin(p, false);			// Set output to low (TODO check if thats okay)

	p = pin_clock;
	gpio_setpinconfig(p, true, false, false);	// output, no pullup, no pulldown
	gpio_writepin(p, false);			// Set output to low (TODO check if thats okay)

	p = pin_clear;
	gpio_setpinconfig(p, true, false, false);	// output, no pullup, no pulldown
	gpio_writepin(p, false);			// Set output to low (TODO check if thats okay)
}

void sipo_reset(void)
{
	reset();
}

void sipo_clock(void)
{
	advance_clock();
}

void sipo_write(const bool bit)
{
	writebit(bit);
}
