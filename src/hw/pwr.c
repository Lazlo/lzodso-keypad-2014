/* pwr.c */

#include "pwr.h"

#include <avr/sleep.h>

void pwr_sleep(void)
{
	sleep_cpu();
}

void pwr_sleep_enable(const bool on)
{
	if (on)	sleep_enable();
	else	sleep_disable();
}
