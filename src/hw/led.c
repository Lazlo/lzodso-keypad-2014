/* led.c */

#include "led.h"

static struct pin_s *pin_led1;
static bool g_led1_on;

void ledinit(struct pin_s *p)
{
	pin_led1 = p;

	gpio_setpinconfig(p, true, false, false);	// output, no pullup, no pulldown
	gpio_writepin(p, false);			// LED is low side switched (is active when output is low)
}

void ledset(const bool on)
{
	g_led1_on = on;

	gpio_writepin(pin_led1, g_led1_on);
}

void ledtoggle(void)
{
	ledset(!g_led1_on);
}
