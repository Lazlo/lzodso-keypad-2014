/* main.c */

#include "config.h"
#include "hw/gpio.h"
#include "hw/led.h"
#ifndef CONFIG_NO_SPI
#include "hw/spi.h"
#endif
#include "sys/busif.h"
#include "sys/syslog.h"
#include "sys/sched.h"
#include "os.h"
#include "uptime.h"

/* GPIO Pins Used */

enum sys_gpio_e {
	GPIO_USART0_RXD = 0,
	GPIO_USART0_TXD,
	GPIO_LED1,
	GPIO_KBD_INT,
	GPIO_SPI_SCK,
	GPIO_SPI_MISO,
	GPIO_SPI_MOSI,
	GPIO_SPI_SS,
};

/* System GPIOs */

#include <avr/io.h>

#define PIN(port, num)		{num, &DDR##port, &PIN##port, &PORT##port}

static struct pin_s g_sys_gpio[] = {
	[GPIO_USART0_RXD]	= PIN(D, 0), /* PD0 */
	[GPIO_USART0_TXD]	= PIN(D, 1), /* PD1 */
	[GPIO_KBD_INT]		= PIN(D, 2), /* PD2 */
	[GPIO_LED1]		= PIN(D, 3), /* PD3 */
	[GPIO_SPI_SCK]		= PIN(B, 5), /* PB5 */
	[GPIO_SPI_MISO]		= PIN(B, 4), /* PB4 */
	[GPIO_SPI_MOSI]		= PIN(B, 3), /* PB3 */
	[GPIO_SPI_SS]		= PIN(B, 2), /* PB2 */
};

#ifndef CONFIG_NO_SPI
/* SPI master mode */

static struct spi_cfg_s g_spicfg = {
	.master		= true,
	.sckmode	= 0,
	.sckrate	= 3,	/* F_OSC /128 */
	.lsbfirst	= false,
};

static struct spi_pins_s g_spipins = {
	.sck		= &g_sys_gpio[GPIO_SPI_SCK],
	.miso		= &g_sys_gpio[GPIO_SPI_MISO],
	.mosi		= &g_sys_gpio[GPIO_SPI_MOSI],
	.ss		= &g_sys_gpio[GPIO_SPI_SS],
};
#endif

/* Bus Interface IO operations */

static struct busif_io_s g_busif_io = {
	.update		= spi_update,
	.rxready	= spi_rxready,
	.read		= spi_read,
	.txready	= spi_txready,
	.write		= spi_write,
};

static void initboard(void);
static void addtasks(void);

#ifndef CONFIG_NO_LOGREADY
#define log_ready(s)	syslog(s " ready\r\n")
#else
#define log_ready(s)
#endif

static void initboard(void)
{
	/* Initialize LED */

	ledinit(&g_sys_gpio[GPIO_LED1]);

#ifndef CONFIG_NO_BUSIF
#ifndef CONFIG_NO_SPI
	/* Setup the SPI module as master */

	spi_init(&g_spicfg, &g_spipins);
	log_ready("spi");
#endif

	/* Setup the bus interface abstraction */

	busif_init(&g_busif_io, &g_sys_gpio[GPIO_KBD_INT], g_spicfg.master);
	log_ready("busif");
#endif
}

static void addtasks(void)
{
	/* Setup tasks */

	sched_addtask(ledtoggle,	0,  500);
#ifndef CONFIG_NO_UPTIME
	sched_addtask(uptime_show,	0, 1000);
#endif
	sched_addtask(busif_update,	0,  250);

	ledset(true);
}

int main(void)
{
	os_init();
	/* Initialize board support */
	initboard();
	addtasks();
	os_start();
}
