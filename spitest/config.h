#pragma once

/* config.h */

/* Firmware Banner String */

#define FW_BANNER "20150128-lzodso-spimaster-test"

/* UART Baud Rate */

#define CONFIG_UARTBAUD			115200

/* TIMER1 Settings */

/* Scheduler Options */

#define CONFIG_SCHED_TASKS_MAX		8

/* SPI Settings */

/* Bus Interface Options */

#define CONFIG_BUSIF_TXBUFSIZE		8

/* Console Options */

#define CONFIG_CONSOLE_TXBUFSIZE	255
#define CONFIG_CONSOLE_RXBUFSIZE	15

/* Development Options */

//#define CONFIG_DEBUG_TIMER1
//#define CONFIG_DEBUG_SPI
//#define CONFIG_DEBUG_BUSIF

//#define CONFIG_NO_LOGREADY
//#define CONFIG_NO_UPTIME
//#define CONFIG_NO_SPI
//#define CONFIG_NO_BUSIF

/* Simulator Options */

//#define CONFIG_SIMULAVR

#define CONFIG_SIMULAVR_OUT_REG		0x20
#define CONFIG_SIMULAVR_IN_REG		0x22
