#!/bin/bash

set -e
set -u

input_dirs="include src"
output_xml="cloc.xml"
output_sc="sloccount.sc"

cloc_opts_default="--by-file"
cloc_opts_xml="--xml -out=$output_xml"

# Run cloc and show output
cloc $cloc_opts_default $input_dirs

# Run it again to generate XML output
# for further processing
cloc $cloc_opts_default $cloc_opts_xml $input_dirs

xsltproc $(dirname $0)/cloc2sloccount.xsl $output_xml > $output_sc
