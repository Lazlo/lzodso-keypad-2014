#pragma once

/* timer1.h */

void timer1_init(void);

void timer1_sethandler(void (*handler)(void));
