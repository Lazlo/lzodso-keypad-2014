#pragma once

/* led.h */

#include "gpio.h"

void ledinit(struct pin_s *pin);

void ledset(const bool on);

void ledtoggle(void);
