#pragma once

/* sipo.h */

#include "gpio.h"

void sipo_init(struct pin_s *pin_dataout,
		struct pin_s *pin_clock,
		struct pin_s *pin_clear);

void sipo_reset(void);
void sipo_clock(void);
void sipo_write(const bool bit);
