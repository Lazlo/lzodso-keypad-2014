#pragma once

/* busif.h */

#include "gpio.h"

#include <stdbool.h>
#include <stdint.h>

struct busif_io_s {
	void (*update)(void);
	bool (*rxready)(void);
	uint8_t (*read)(void);
	bool (*txready)(void);
	void (*write)(const uint8_t);
};

void busif_init(struct busif_io_s *io, struct pin_s *pin_intout, bool master);

void busif_update(void);

void busif_put(const uint8_t b);
