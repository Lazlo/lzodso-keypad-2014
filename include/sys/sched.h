#pragma once

/* sched.h */

#include <stdint.h>

void sched_init(void);
int sched_addtask(void (*fp)(void), const uint16_t delay, const uint16_t periode);
void sched_start(void);
void sched_update(void);
void sched_dispatch(void);
