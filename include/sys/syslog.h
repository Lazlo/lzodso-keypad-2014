#pragma once

/* syslog.h */

void syslog_init(void (*putc)(const char s));

void syslog(char *msg);
