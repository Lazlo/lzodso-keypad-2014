#pragma once

/* console.h */

void console_init(void (*fp_putc)(const char), char (*fp_getc)(void));

void console_putc(const char c);

char console_getc(void);

void console_update(void);
