#pragma once

/* config.h */

/* Firmware Banner String */

#define FW_BANNER "lzodso-keypad-2014"

/* UART Baud Rate
 *
 * Possible baud rates at 20 MHz:
 *
 *   115200 (default)
 *    57600
 *    38400
 *    19200
 *     9600
 */

#define CONFIG_UARTBAUD			115200

/* TIMER1 Settings */

/* Scheduler Options */

#define CONFIG_SCHED_TASKS_MAX		8

/* TWI Slave Address */

#define CONFIG_TWIADDR			0x15

/* SPI Settings */

/* Bus Interface Options */

#define CONFIG_BUSIF_TXBUFSIZE		8

/* lzoDSO Keyboard Options */

/* Keyboard Options */

#define CONFIG_KBD_KEYBUFSIZE		8

/* Console Options */

#define CONFIG_CONSOLE_TXBUFSIZE	255
#define CONFIG_CONSOLE_RXBUFSIZE	15

/* Development Options */

//#define CONFIG_DEBUG_TWI
//#define CONFIG_DEBUG_SPI
//#define CONFIG_DEBUG_TIMER1
//#define CONFIG_DEBUG_BUSIF
//#define CONFIG_DEBUG_LDK
#define CONFIG_DEBUG_KBD
#define CONFIG_DEBUG_NETKBD

#define CONFIG_NO_LOGREADY
#define CONFIG_NO_UPTIME
#define CONFIG_NO_TWI
//#define CONFIG_NO_SPI

/* Simulator Options */

//#define CONFIG_SIMULAVR

#define CONFIG_SIMULAVR_OUT_REG		0x20
#define CONFIG_SIMULAVR_IN_REG		0x22
