#pragma once

/* rb.h */

#include <stdbool.h>
#include <stdint.h>

struct rb_s {
	char *buf;		/**< Pointer to ring buffer memory. */
	unsigned char len;	/**< Lenght of ring buffer memory. */
	unsigned char head;	/**< Head cursor position within buffer. */
	unsigned char tail;	/**< Tail cursor position within buffer. */
};

inline void rb_flush(struct rb_s *b)
{
	b->head = b->tail = 0;
}

inline void rb_init(struct rb_s *b)
{
	rb_flush(b);
}

inline bool rb_empty(struct rb_s *b)
{
	return b->head < b->tail ? false : true;
}

inline bool rb_full(struct rb_s *b)
{
	return b->tail < b->len ? false : true;
}

inline char rb_get(struct rb_s *b)
{
	char e;

	e = b->buf[b->head++];

	if (b->head == b->tail)
		rb_flush(b);

	return e;
}

inline void rb_put(struct rb_s *b, const uint8_t e)
{
	if (b->head == b->tail)
		rb_flush(b);

	b->buf[b->tail] = e;

	if (!rb_full(b))
		b->tail++;
}
